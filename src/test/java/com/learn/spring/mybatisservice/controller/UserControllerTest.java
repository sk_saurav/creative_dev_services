/*
 * Copyright for UserControllerTest.java by kumar since 9/6/20, 4:46 PM
 */

package com.learn.spring.mybatisservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.learn.spring.mybatisservice.MybatisServiceApplicationTests;
import com.learn.spring.mybatisservice.entity.Users;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author kumar
 * @project mybatis-service
 * @since 9/6/2020
 */
class UserControllerTest extends MybatisServiceApplicationTests {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllIndustry() throws Exception {
        mockMvc.perform(get("/getIndustryInfoData"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
    }

    @Test
    void getUserCache() throws Exception {
        mockMvc.perform(get("/getUserInfoData"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
    }

    @Test
    void getCommonProperties() {
    }

    @Test
    void getIndusTypeInfo() {
    }

    @Test
    void getUserInfoData() {
        final List<Integer> numbers = Arrays.asList(2, 3, 4, 5);
        Assertions.assertAll(() -> Assertions.assertEquals(2, numbers.get(0)),
                () -> Assertions.assertEquals(3, numbers.get(1)),
                () -> Assertions.assertEquals(4, numbers.get(2)),
                () -> Assertions.assertEquals(5, numbers.get(3)));
    }

    @Test
    @DisplayName("getEnv")
    void getEnv() throws Exception {
        mockMvc.perform(get("/getEnv").param("env", "nsp.app.id"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN_VALUE + ";charset=UTF-8"))
                .andReturn();
    }
    @Test
    @DisplayName("deleteMethod")
    void deleteMethod () throws Exception{
        mockMvc.perform(delete("/contentDelete").param("contentID", "4877658"))
                .andExpect(status().isAccepted())
                //.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON + ";charset=UTF-8"))
                .andReturn();
    }
    @Test
    @DisplayName("addUsers")
    void addUsers () throws Exception{
        List<Users> usersList = new ArrayList<>();
        Users users1 = new Users("653754", "saur345", "gfdsf@jgd.com", "74765d87765");
        Users users2 = new Users("6567754", "saur3456", "gfdssf@jgd.com", "747654587765");
        usersList.add(users2);
        usersList.add(users1);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(usersList);
        mockMvc.perform(post("/v1/addUsers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                //.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON + ";charset=UTF-8"))
                .andReturn();
    }
    @Test
    @DisplayName("getConfigValue")
    void getConfigValue () throws Exception {
        mockMvc.perform(get("/getEnv/{env}", "nsp.app.id"))
                .andExpect(status().isOk())
                .andReturn();
    }
}
