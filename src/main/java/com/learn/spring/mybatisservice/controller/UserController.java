/*
 * Copyright for UserController.java by kumar since 26/09/20, 5:51 PM
 */
package com.learn.spring.mybatisservice.controller;

import com.learn.spring.mybatisservice.common.bean.CommonProperties;
import com.learn.spring.mybatisservice.common.cache.CacheConfigVo;
import com.learn.spring.mybatisservice.common.cache.StaticCacheConfig;
import com.learn.spring.mybatisservice.entity.ContentVO;
import com.learn.spring.mybatisservice.entity.IndusTypeInfo;
import com.learn.spring.mybatisservice.entity.OrderHeadVO;
import com.learn.spring.mybatisservice.entity.Users;
import com.learn.spring.mybatisservice.response.ResultResponse;
import com.learn.spring.mybatisservice.service.OrderService;
import com.learn.spring.mybatisservice.service.UserAddService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * User controller to record User based request
 *
 * @author kumar
 * @project mybatis -service
 * @since 8 /1/2020
 */
@RestController
public class UserController {

    /**
     * The constant THANKS.
     */
    public static final String THANKS = "Thanks";
    /**
     * logger added to record logs
     */
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    /**
     * user service impl to record
     */
    @Autowired
    private UserAddService userAddService;

    /**
     * The Config vo.
     */
    @Autowired
    private CacheConfigVo configVo;
    /**
     * The Cache config.
     */
    @Qualifier("staticCacheConfig")
    @Autowired
    private StaticCacheConfig cacheConfig;
    /**
     * The Order service.
     */
    @Autowired
    private OrderService orderService;

    /**
     * Add users response entity.
     *
     * @param usersVo the users vo
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping("/v1/addUsers")
    public ResponseEntity<ResultResponse<Users>> addUsers(@RequestBody List<Users> usersVo)
            throws Exception {
        logger.info("Entering into addUsers service with userVo: {}.", usersVo);
        ResultResponse<Users> userInfo = userAddService.addUsers(usersVo);
        return ResponseEntity.ok(userInfo);
    }

    /**
     * Gets all industry.
     *
     * @return the all industry
     */
    @GetMapping("/getAllIndustry")
    public Map<Integer, IndusTypeInfo> getAllIndustry() {
        return userAddService.getAllIndustry();
    }

    /**
     * Gets generated key.
     *
     * @param users the users
     * @return the generated key
     */
    @GetMapping("getGeneratedKey")
    public int getGeneratedKey(Users users) {
        return userAddService.getGeneratedKey(users);
    }

    /**
     * Gets user cache.
     *
     * @return the user cache
     */
    @GetMapping("/userCache")
    public ResponseEntity<Map<String, Users>> getUserCache() {
        return ResponseEntity.ok(configVo.getUserCache());
    }

    /**
     * Gets common properties.
     *
     * @return the common properties
     */
//    @GetMapping("/common/properties")
//    public ResponseEntity<Map<String, String>> getCommonProperties() {
//        return userAddService.getCommonProperties();
//    }

    /**
     * Gets indus type info.
     *
     * @return the indus type info
     */
    @GetMapping("/getIndustryInfoData")
    public Map<String, IndusTypeInfo> getIndusTypeInfo() {
        return cacheConfig.getIndusTypeCache();
    }

    /**
     * Gets user info data.
     *
     * @return the user info data
     */
    @GetMapping("/getUserInfoData")
    public Map<String, Users> getUserInfoData() {
        return cacheConfig.getUsersCache();
    }

    /**
     * Content delete response entity.
     *
     * @param contentID the content id
     * @return the response entity
     */
    @DeleteMapping("/contentDelete")
    public ResponseEntity<String> contentDelete(@RequestParam("contentID") String contentID) {
        return userAddService.contentDelete(contentID);
    }

    /**
     * Library content response entity.
     *
     * @param contentIDList the content id list
     * @return the response entity
     */
    @GetMapping("/lambda")
    public ResponseEntity<ResultResponse<List<ContentVO>>> libraryContent(@RequestParam("contentIDList") List<String> contentIDList) {
        return userAddService.libraryContent(contentIDList);
    }


    /**
     * Create order response entity.
     *
     * @param orderHeadVOList the order head vo list
     * @return the response entity
     */
    @PostMapping("/createOrder")
    public ResponseEntity<String> createOrder(@RequestBody(required = true) List<OrderHeadVO> orderHeadVOList) {
        return orderService.createOrder(orderHeadVOList);
    }

    @GetMapping("/commonKey/{key}")
    public String commonKey(@PathVariable("key") final String key) {
        return CommonProperties.getCommonProperty(key);
    }

}
