/*
 * Copyright for MybatisServiceApplication.java by kumar since 26/09/20, 5:58 PM
 */

package com.learn.spring.mybatisservice;

import com.learn.spring.mybatisservice.common.cache.StaticCacheConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * The type Mybatis service application.
 */
@SpringBootApplication
@MapperScan("com.learn.spring.mybatisservice.dao")
@EnableAsync
@EnableCaching
public class MybatisServiceApplication {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(MybatisServiceApplication.class, args);
    }

    /**
     * Task executors executor.
     *
     * @return the executor
     */
    @Bean
    public Executor taskExecutors() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("GithubLookup-");
        executor.initialize();
        return executor;
    }

    /**
     * Cache config list.
     *
     * @return the list
     */
    @Bean
    public List<String> cacheConfig() {
        List<String> cacheList = new ArrayList<>();
        //cacheList.add("staticCacheConfig");
        cacheList.add("initializeCache");
        return cacheList;
    }

    /**
     * User cache static cache config.
     *
     * @return the static cache config
     */
    @Bean(name = "loadUserCache", initMethod = "loadUserCache")
    public StaticCacheConfig userCache() {
        return new StaticCacheConfig();
    }

    /**
     * Dimension cache static cache config.
     *
     * @return the static cache config
     */
    @Bean(name = "loadDimensionCache", initMethod = "loadDimensionCache")
    public StaticCacheConfig dimensionCache() {
        return new StaticCacheConfig();
    }

    @Bean("PropertiesFactoryBean")
    public PropertiesFactoryBean propertiesFactoryBean() {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        try {
            final List<Resource> resourceList = new ArrayList<>();
            resourceList.addAll(Arrays.asList(patternResolver.getResources("classpath:*.properties")));
            resourceList.addAll(Arrays.asList(patternResolver.getResources("classpath:*.yaml")));
            propertiesFactoryBean.setFileEncoding("UTF-8");
            propertiesFactoryBean.setLocations(resourceList.toArray(new Resource[]{}));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return propertiesFactoryBean;
    }

}
