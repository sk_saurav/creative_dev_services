/*
 * Copyright for OrderHeadVO.java by kumar since 19/09/20, 8:48 PM
 */

package com.learn.spring.mybatisservice.entity;

import lombok.*;

/**
 * @author kumar
 * @project mybatis-service
 * @since 17-09-2020
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OrderHeadVO {
    private String orderId;
    private String ioaId;
    private String orderNumber;
}
