/*
 * Copyright for UserMock.java by kumar since 26/09/20, 12:32 PM
 */

package com.learn.spring.mybatisservice.entity;

import lombok.*;

/**
 * @author kumar
 * @project mybatis-service
 * @since 20-09-2020
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class UserMock {
    private String id;
    private String first_name;
    private String last_name;
    private String gender;
    private String email;
    private String ip_address;
    private String name;
}
