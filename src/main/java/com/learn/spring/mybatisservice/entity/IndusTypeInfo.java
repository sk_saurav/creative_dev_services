/*
 * Copyright for IndusTypeInfo.java by kumar since 9/6/20, 12:54 PM
 */

package com.learn.spring.mybatisservice.entity;

/**
 * The type Indus type info.
 *
 * @author kumar
 * @project mybatis -service
 * @since 8 /5/2020
 */
public class IndusTypeInfo {

    private int indusTypeId;
    private String indusTypeName;

    /**
     * Instantiates a new Indus type info.
     */
    public IndusTypeInfo() {
    }

    /**
     * Instantiates a new Indus type info.
     *
     * @param indusTypeId   the indus type id
     * @param indusTypeName the indus type name
     */
    public IndusTypeInfo(int indusTypeId, String indusTypeName) {
        this.indusTypeId = indusTypeId;
        this.indusTypeName = indusTypeName;
    }

    /**
     * Gets indus type id.
     *
     * @return the indus type id
     */
    public int getIndusTypeId() {
        return indusTypeId;
    }

    /**
     * Sets indus type id.
     *
     * @param indusTypeId the indus type id
     */
    public void setIndusTypeId(int indusTypeId) {
        this.indusTypeId = indusTypeId;
    }

    /**
     * Gets indus type name.
     *
     * @return the indus type name
     */
    public String getIndusTypeName() {
        return indusTypeName;
    }

    /**
     * Sets indus type name.
     *
     * @param indusTypeName the indus type name
     */
    public void setIndusTypeName(String indusTypeName) {
        this.indusTypeName = indusTypeName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IndusTypeInfo{");
        sb.append("indusTypeId=").append(indusTypeId);
        sb.append(", indusTypeName=").append(indusTypeName);
        sb.append('}');
        return sb.toString();
    }
}
