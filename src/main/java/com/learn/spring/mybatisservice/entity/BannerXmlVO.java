/*
 * Copyright for BannerXmlVO.java by kumar since 8/16/20, 12:39 AM
 */

package com.learn.spring.mybatisservice.entity;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * The type Banner xml vo.
 *
 * @project mybatis -service
 * @uthor kumar
 * @since 7 /25/2020
 */
@XmlRootElement(name = "IntelligentAttribute")
public class BannerXmlVO {
    private String imageUrl;
    private String imageLogo;
    private Heading headings;
    private List<DimensionVO>elements;
    private Integer width;
    private Integer height;
    private String outputPath;

    /**
     * Gets image url.
     *
     * @return the image url
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets image url.
     *
     * @param imageUrl the image url
     */
    @XmlTransient
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Gets image logo.
     *
     * @return the image logo
     */
    @XmlTransient
    public String getImageLogo() {
        return imageLogo;
    }

    /**
     * Sets image logo.
     *
     * @param imageLogo the image logo
     */
    public void setImageLogo(String imageLogo) {
        this.imageLogo = imageLogo;
    }

    /**
     * Gets headings.
     *
     * @return the headings
     */
    @XmlTransient
    public Heading getHeadings() {
        return headings;
    }

    /**
     * Sets headings.
     *
     * @param headings the headings
     */
    public void setHeadings(Heading headings) {
        this.headings = headings;
    }

    /**
     * Gets elements.
     *
     * @return the elements
     */
    @XmlElement(name = "element")
    public List<DimensionVO> getElements() {
        return elements;
    }

    /**
     * Sets elements.
     *
     * @param elements the elements
     */
    public void setElements(List<DimensionVO> elements) {
        this.elements = elements;
    }

    /**
     * Gets width.
     *
     * @return the width
     */
    @XmlAttribute(name = "width", required = true)
    public Integer getWidth() {
        return width;
    }

    /**
     * Sets width.
     *
     * @param width the width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * Gets height.
     *
     * @return the height
     */
    @XmlAttribute(name = "height", required = false)
    public Integer getHeight() {
        return height;
    }

    /**
     * Sets height.
     *
     * @param height the height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * Gets output path.
     *
     * @return the output path
     */
    @XmlAttribute(name = "outputPath", required = true)
    public String getOutputPath() {
        return outputPath;
    }

    /**
     * Sets output path.
     *
     * @param outputPath the output path
     */
    public void setOutputPath(String outputPath) {
        this.outputPath = outputPath;
    }

}
