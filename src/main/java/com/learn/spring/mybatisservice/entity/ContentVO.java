/*
 * Copyright for ContentVO.java by kumar since 9/13/20, 12:19 PM
 */

package com.learn.spring.mybatisservice.entity;

/**
 * @author kumar
 * @project mybatis-service
 * @since 9/13/2020
 */
public class ContentVO {
    private int contentID;
    private String contentOwner;
    private String contentUrl;

    public ContentVO(int contentID, String contentOwner, String contentUrl) {
        this.contentID = contentID;
        this.contentOwner = contentOwner;
        this.contentUrl = contentUrl;
    }

    public ContentVO() {
    }

    public int getContentID() {
        return contentID;
    }

    public void setContentID(int contentID) {
        this.contentID = contentID;
    }


    public String getContentOwner() {
        return contentOwner;
    }

    public void setContentOwner(String contentOwner) {
        this.contentOwner = contentOwner;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("contentID=").append(contentID);
        sb.append(", contentOwner=").append(contentOwner);
        sb.append(", contentUrl=").append(contentUrl);
        sb.append('}');
        return sb.toString();
    }
}
