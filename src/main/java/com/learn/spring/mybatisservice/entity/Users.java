/*
 * Copyright for Users.java by kumar since 8/16/20, 12:39 AM
 */

package com.learn.spring.mybatisservice.entity;

/**
 * keeping user details
 *
 * @author kumar
 * @project mybatis -service
 * @since 8 /1/2020
 */
public class Users {
    /**
     * ID
     */
    private int ID;
    /**
     * contains user id
     */
    private String userID;

    /**
     * contains user name
     */
    private String userName;

    /**
     * contains user email
     */
    private String userEmail;

    /**
     * contains user contacts
     */
    private String userContact;

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets user id.
     *
     * @param userID the user id
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * Gets user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets user name.
     *
     * @param userName the user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets user email.
     *
     * @return the user email
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Sets user email.
     *
     * @param userEmail the user email
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * Gets user contact.
     *
     * @return the user contact
     */
    public String getUserContact() {
        return userContact;
    }

    /**
     * Sets user contact.
     *
     * @param userContact the user contact
     */
    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getID() {
        return ID;
    }

    /**
     * Sets id.
     *
     * @param ID the id
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    public Users(String userID, String userName, String userEmail, String userContact) {
        this.userID = userID;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userContact = userContact;
    }

    /**
     * {@utility method}
     *
     * @return
     */


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Users{");
        stringBuilder.append("userID=" + userID);
        stringBuilder.append(", userName=" + userName);
        stringBuilder.append(", userEmail=" + userEmail);
        stringBuilder.append(", userContact=" + userContact);
        stringBuilder.append("}");
        return stringBuilder.toString();
    }
}
