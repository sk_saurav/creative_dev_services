/*
 * Copyright for User.java by kumar since 8/30/20, 8:32 PM
 */

package com.learn.spring.mybatisservice.entity;

import lombok.*;

/**
 * The type User.
 *
 * @author Saurav Kumar
 * @project mybatis -service
 * @since 7 /15/2020
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@NonNull
@ToString
public class User {
    private String name;
    private String blog;
    private Long userId;
}
