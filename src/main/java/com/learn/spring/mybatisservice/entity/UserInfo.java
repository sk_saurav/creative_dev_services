/*
 * Copyright for UserInfo.java by kumar since 8/16/20, 12:39 AM
 */

package com.learn.spring.mybatisservice.entity;


import lombok.*;

/**
 * The type User info.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@NonNull
@ToString
public class UserInfo {
    private Long userId;
    private String userName;
    private Long userContact;
    private String userEmail;
    private String name;
    private String blog;
}
