/*
 * Copyright for Heading.java by kumar since 8/16/20, 12:39 AM
 */

package com.learn.spring.mybatisservice.entity;

/**
 * The type Heading.
 *
 * @author saurav kumar
 * @since 8 /8/2020
 */
public class Heading {
    private String mainText;

    private String subtitle;

    private String summary;

    /**
     * Gets main text.
     *
     * @return the main text
     */
    public String getMainText() {
        return mainText;
    }

    /**
     * Sets main text.
     *
     * @param mainText the main text
     */
    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    /**
     * Gets subtitle.
     *
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * Sets subtitle.
     *
     * @param subtitle the subtitle
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * Gets summary.
     *
     * @return the summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Sets summary.
     *
     * @param summary the summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }
}
