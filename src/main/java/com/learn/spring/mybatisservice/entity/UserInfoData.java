/*
 * Copyright for UserInfoData.java by kumar since 9/6/20, 12:54 PM
 */

package com.learn.spring.mybatisservice.entity;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * The type User info data.
 *
 * @author kumar
 * @project mybatis -service
 * @since 8 /1/2020
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserInfoData {
    private String userID;
    private String userName;
    private String userEmail;
    private String userContact;

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets user id.
     *
     * @param userID the user id
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * Gets user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets user name.
     *
     * @param userName the user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets user email.
     *
     * @return the user email
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Sets user email.
     *
     * @param userEmail the user email
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * Gets user contact.
     *
     * @return the user contact
     */
    public String getUserContact() {
        return userContact;
    }

    /**
     * Sets user contact.
     *
     * @param userContact the user contact
     */
    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    /**
     * Instantiates a new User info data.
     */
    public UserInfoData() {
    }

    /**
     * Instantiates a new User info data.
     *
     * @param userID      the user id
     * @param userName    the user name
     * @param userEmail   the user email
     * @param userContact the user contact
     */
    public UserInfoData(String userID, String userName, String userEmail, String userContact) {
        this.userID = userID;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userContact = userContact;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserInfoData{");
        sb.append("userID=").append(userID);
        sb.append(", userName=").append(userName);
        sb.append(", userEmail=").append(userEmail);
        sb.append(", userContact=").append(userContact);
        sb.append('}');
        return sb.toString();
    }
}
