/*
 * Copyright for DimensionVO.java by kumar since 9/6/20, 12:54 PM
 */

package com.learn.spring.mybatisservice.entity;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * The type Dimension vo.
 *
 * @project mybatis -service
 * @uthor kumar
 * @since 7 /25/2020
 */
public class DimensionVO {
    private String dimensionName;
    private Integer width;
    private Integer height;
    private String mainTitle;
    private String subTitle;
    private String description;
    private String inputPath;
    private String imageUrls;
    private String logoUrls;

    /**
     * Gets dimension name.
     *
     * @return the dimension name
     */
    @XmlAttribute(name = "dimension", required = true)
    public String getDimensionName() {
        return dimensionName;
    }

    /**
     * Sets dimension name.
     *
     * @param dimensionName the dimension name
     */
    public void setDimensionName(String dimensionName) {
        this.dimensionName = dimensionName;
    }

    /**
     * Gets width.
     *
     * @return the width
     */
    @XmlAttribute(name = "width", required = true)
    public Integer getWidth() {
        return width;
    }

    /**
     * Sets width.
     *
     * @param width the width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * Gets height.
     *
     * @return the height
     */
    @XmlAttribute(name = "height", required = true)
    public Integer getHeight() {
        return height;
    }

    /**
     * Sets height.
     *
     * @param height the height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * Gets main title.
     *
     * @return the main title
     */
    @XmlAttribute(name = "mainTitle")
    public String getMainTitle() {
        return mainTitle;
    }

    /**
     * Sets main title.
     *
     * @param mainTitle the main title
     */
    public void setMainTitle(String mainTitle) {
        this.mainTitle = mainTitle;
    }

    /**
     * Gets sub title.
     *
     * @return the sub title
     */
    @XmlAttribute(name = "subTitle")
    public String getSubTitle() {
        return subTitle;
    }

    /**
     * Sets sub title.
     *
     * @param subTitle the sub title
     */
    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    @XmlAttribute(name = "description")
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets input path.
     *
     * @return the input path
     */
    @XmlAttribute (name = "imagePath")
    public String getInputPath() {
        return inputPath;
    }

    /**
     * Sets input path.
     *
     * @param inputPath the input path
     */
    public void setInputPath(String inputPath) {
        this.inputPath = inputPath;
    }

    /**
     * Gets image urls.
     *
     * @return the image urls
     */
    @XmlAttribute(name = "imageUrls" ,required = true)
    public String getImageUrls() {
        return imageUrls;
    }

    /**
     * Sets image url.
     *
     * @param imageUrls the image urls
     */
    public void setImageUrl(String imageUrls) {
        this.imageUrls = imageUrls;
    }

    /**
     * Gets logo urls.
     *
     * @return the logo urls
     */
    @XmlAttribute(name = "logoUrls" ,required = true)
    public String getLogoUrls() {
        return logoUrls;
    }

    /**
     * Sets logo urls.
     *
     * @param logoUrls the logo urls
     */
    public void setLogoUrls(String logoUrls) {
        this.logoUrls = logoUrls;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DimensionVO{");
        sb.append("dimensionName='").append(dimensionName).append('\'');
        sb.append(", width=").append(width);
        sb.append(", height=").append(height);
        sb.append(", mainTitle=").append(mainTitle);
        sb.append(", subTitle=").append(subTitle);
        sb.append(", description=").append(description);
        sb.append(", inputPath=").append(inputPath);
        sb.append(", imageUrls=").append(imageUrls);
        sb.append(", logoUrls=").append(logoUrls);
        sb.append('}');
        return sb.toString();
    }
}
