/*
 * Copyright for DeleteResponseVO.java by kumar since 26/09/20, 12:32 PM
 */

package com.learn.spring.mybatisservice.entity;

/**
 * @author kumar
 * @project mybatis-service
 * @since 24-09-2020
 */
public class DeleteResponseVO {
    private String code;
    private String description;
    private String iD;

    public DeleteResponseVO() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getiD() {
        return iD;
    }

    public void setiD(String iD) {
        this.iD = iD;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DeleteResponseVO{");
        sb.append("code=").append(code);
        sb.append(", description=").append(description);
        sb.append(", iD=").append(iD);
        sb.append('}');
        return sb.toString();
    }
}
