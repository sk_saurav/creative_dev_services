/*
 * Copyright for IdNotFoundException.java by kumar since 8/16/20, 12:39 AM
 */

package com.learn.spring.mybatisservice.common.exception;

/**
 * Created By Saurav Kumar on 7/7/2020
 */
public class IdNotFoundException extends RuntimeException {
    /**
     * Instantiates a new Id not found exception.
     *
     * @param msg the msg
     */
    public IdNotFoundException(String msg) {
        super(msg);
    }
}
