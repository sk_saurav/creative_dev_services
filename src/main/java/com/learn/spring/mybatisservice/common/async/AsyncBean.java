/*
 * Copyright for AsyncBean.java by kumar since 26/09/20, 12:32 PM
 */

package com.learn.spring.mybatisservice.common.async;

import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;

/**
 * @author kumar
 * @project mybatis-service
 * @since 20-09-2020
 */
@Component
@EnableAsync
public class AsyncBean {
    @Bean(name = "taskExecutor")
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(100);
        taskExecutor.setCorePoolSize(2);
        taskExecutor.setQueueCapacity(100);
        taskExecutor.setThreadNamePrefix(" > Task Async ");
        return taskExecutor;
    }
}
