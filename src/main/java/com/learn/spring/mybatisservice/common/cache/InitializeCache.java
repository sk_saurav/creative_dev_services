/*
 * Copyright for InitializeCache.java by kumar since 8/16/20, 12:39 AM
 */

/*
 * Copy right from Saurav kumar
 */

package com.learn.spring.mybatisservice.common.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * The type Initialize cache.
 *
 * @author kumar
 * @since 8 /8/2020
 */
@Component(value = "initializeCache")
public class InitializeCache {
    /**
     * logger instance for methods
     */
    private static final Logger logger = LoggerFactory.getLogger(InitializeCache.class);

}
