/*
 * Copyright for SpringFactoryBean.java by kumar since 26/09/20, 5:51 PM
 */

package com.learn.spring.mybatisservice.common.bean;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author kumar
 * @project mybatis-service
 * @since 26-09-2020
 */
@Component
public class SpringFactoryBean implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    public SpringFactoryBean() {
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        setApplicationContextValue(applicationContext);
    }

    private static void setApplicationContextValue(ApplicationContext applicationContext) {
        SpringFactoryBean.applicationContext = applicationContext;
    }

    public static <T> T getBean(String name) {
        return (T) getApplicationContext().getBean(name);
    }
}
