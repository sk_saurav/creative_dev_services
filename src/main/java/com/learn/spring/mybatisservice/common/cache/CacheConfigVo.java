/*
 * Copyright for CacheConfigVo.java by kumar since 9/6/20, 12:54 PM
 */

/*
 * Copy right from Saurav kumar
 */

package com.learn.spring.mybatisservice.common.cache;

import com.learn.spring.mybatisservice.entity.IndusTypeInfo;
import com.learn.spring.mybatisservice.entity.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Cache config vo.
 *
 * @author kumar
 * @project mybatis -service
 * @since 8 /8/2020
 */
@Component()
@DependsOn({"loadUserCache", "loadDimensionCache"})
public class CacheConfigVo {
    /** logger instance of the class */
    private static final Logger log = LoggerFactory.getLogger(StaticCacheConfig.class);

    /**
     * user cache
     */
    private Map<String, Users> userCache = new HashMap<>();

    /**
     * dimension cache
     */
    private Map<String, IndusTypeInfo> indusTypeCache = new HashMap<>();

    /**
     * Gets user cache.
     *
     * @return the user cache
     */
    public Map<String, Users> getUserCache() {
        return userCache;
    }

    /**
     * Sets user cache.
     *
     * @param userCache the user cache
     */
    public void setUserCache(Map<String, Users> userCache) {
        this.userCache = userCache;
    }

    /**
     * Gets indus type cache.
     *
     * @return the indus type cache
     */
    public Map<String, IndusTypeInfo> getIndusTypeCache() {
        return indusTypeCache;
    }

    /**
     * Sets indus type cache.
     *
     * @param indusTypeCache the indus type cache
     */
    public void setIndusTypeCache(Map<String, IndusTypeInfo> indusTypeCache) {
        this.indusTypeCache = indusTypeCache;
    }

    /**
     * Instantiates a new Cache config vo.
     */
    public CacheConfigVo() {}

    /**
     * Instantiates a new Cache config vo.
     *
     * @param userCache      the user cache
     * @param indusTypeCache the indus type cache
     */
    public CacheConfigVo(Map<String, Users> userCache, Map<String, IndusTypeInfo> indusTypeCache) {
        this.userCache = userCache;
        this.indusTypeCache = indusTypeCache;
    }

    /**
     * @{utility method}
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CacheConfigVo{");
        stringBuilder.append("loadUserCache=" + userCache);
        stringBuilder.append(", indusTypeCache=" + indusTypeCache);
        stringBuilder.append('}');
        return stringBuilder.toString();
    }

    /**
     * Gets industry type.
     *
     * @return the industry type
     */
    public Map<String, Users> getIndustryType() {
        return getIndustryType();
    }
}
