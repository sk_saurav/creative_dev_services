/*
 * Copyright for CommonProperties.java by kumar since 26/09/20, 5:51 PM
 */

package com.learn.spring.mybatisservice.common.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * @author kumar
 * @project mybatis-service
 * @since 9/5/2020
 */
public class CommonProperties {
    private static final Logger logger = LoggerFactory.getLogger(CommonProperties.class);
    //private static WebApplicationContext applicationContext;
    private static Properties properties = SpringFactoryBean.getBean("PropertiesFactoryBean");

    public static String getCommonProperty(String keyName) {
        return properties.getProperty(keyName);
    }

    public static Integer getIntegerProperty(String key, int defaultValue) {
        int value = defaultValue;
        try {
            String val = properties.getProperty(key);
            value = Integer.parseInt(val);
        } catch (NumberFormatException e) {
            logger.error("System property Transform the value of key {}., to Integer failed", key);
        }
        return value;
    }
}
