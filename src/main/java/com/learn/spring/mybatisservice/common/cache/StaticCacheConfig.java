/*
 * Copyright for StaticCacheConfig.java by kumar since 9/6/20, 12:54 PM
 */

/*
 * Copy right from Saurav kumar
 */

package com.learn.spring.mybatisservice.common.cache;

import com.learn.spring.mybatisservice.dao.UserDao;
import com.learn.spring.mybatisservice.dao.UserInfoDao;
import com.learn.spring.mybatisservice.entity.IndusTypeInfo;
import com.learn.spring.mybatisservice.entity.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toMap;

/**
 * The type Static cache config.
 *
 * @author kumar
 * @project mybatis -service
 * @since 8 /8/2020
 */
@Component(value = "staticCacheConfig")
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class StaticCacheConfig {
    /**
     * logger instance of the class
     */
    private static final Logger log = LoggerFactory.getLogger(StaticCacheConfig.class);

    /**
     * The Users map.
     */
    Map<String, Users> usersCacheMap = new ConcurrentHashMap<>();

    /**
     * The Indus type info map.
     */
    Map<String, IndusTypeInfo> indusTypeInfoMap = new ConcurrentHashMap<>();

    /**
     * The Users map.
     */
    Map<String, Users> cachedUsers = new ConcurrentHashMap<>();
    CacheConfigVo cacheConfigVo = new CacheConfigVo();
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserInfoDao userInfoDao;

    /**
     * Load user cache boolean.
     *
     * @return the boolean
     */
    @Order(0)
    @PostConstruct
    public boolean loadUserCache() {
        log.info("userConfigCache invoked successfully...");
        List<Users> userList = null;
        // CacheConfigVo cacheConfigVo = null;
        try {
            userList = userDao.findAllUserData();
            if (!CollectionUtils.isEmpty(userList)) {
                usersCacheMap = userList
                        .stream()
                        .collect(
                                toMap(Users::getUserID,
                                        userdata -> userdata));
            }
            cacheConfigVo.setUserCache(usersCacheMap);
        } catch (Exception e) {
            log.error("loadUserCache getting error with DB", e.getMessage());
            return false;
        }
        log.info("Logging user map data {}", usersCacheMap);
        log.info("Returning from userConfigCache ");
        return true;
    }

    /**
     * Load dimension cache boolean.
     *
     * @return the boolean
     */
    @PostConstruct()
    @Order(1)
    public boolean loadDimensionCache() {
        log.info("loadDimensionCache invoked successfully...");
        // CacheConfigVo cacheConfigVo = null;
        List<IndusTypeInfo> indusTypeInfos = null;
        try {
            indusTypeInfos = userInfoDao.getAllIndustry();
            if (!CollectionUtils.isEmpty(indusTypeInfos)) {
                indusTypeInfoMap =
                        indusTypeInfos.stream()
                                .collect(
                                        toMap(
                                                IndusTypeInfo::getIndusTypeName,
                                                industryInfo -> industryInfo));
            }
            cacheConfigVo.setIndusTypeCache(indusTypeInfoMap);
        } catch (Exception e) {
            log.error("loadDimensionCache failed to load data into cache..." + e.getMessage());
            return false;
        }
        log.info("Logging dimension map data {}", indusTypeInfoMap);
        log.info("Returning from loadDimensionCache ");
        return true;
    }


    /**
     * Load user cache boolean.
     *
     * @return the boolean
     */
    @Order(2)
    @PostConstruct()
    public boolean loadUserCacheData() {
        log.info("loadUserCacheData invoked successfully...");
        try {
            List<Users> allUsers = userInfoDao.getAllUsers();
            if (!CollectionUtils.isEmpty(allUsers)) {
                cachedUsers =
                        allUsers.stream()
                                .collect(
                                        toMap(
                                                Users::getUserID,
                                                users -> users));
            }

        } catch (Exception e) {
            log.error("loadUserCache getting error with DB", e.getMessage());
            return false;
        }
        log.info("Logging user map data {}", cachedUsers);
        log.info("Returning from loadUserCacheData ");
        return true;
    }

    /**
     * Gets users cache.
     *
     * @return the users cache
     */
    public Map<String, Users> getUsersCache() {
        return usersCacheMap;
    }

    /**
     * Gets indus type cache.
     *
     * @return the indus type cache
     */
    public Map<String, IndusTypeInfo> getIndusTypeCache() {
        return indusTypeInfoMap;
    }

}
