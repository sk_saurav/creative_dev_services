/*
 * Copyright for ResultResponse.java by kumar since 8/16/20, 12:39 AM
 */

package com.learn.spring.mybatisservice.response;

import org.springframework.http.HttpStatus;

/**
 * The type Result response.
 *
 * @param <T> the type parameter
 * @project mybatis -service
 * @uthor kumar
 * @since 7 /25/2020
 */
public class ResultResponse<T> {
    private char status;
    private String resultMessage;
    private HttpStatus statusCode;
    private T returnValue;

    /**
     * Gets status.
     *
     * @return the status
     */
    public char getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(char status) {
        this.status = status;
    }

    /**
     * Gets result message.
     *
     * @return the result message
     */
    public String getResultMessage() {
        return resultMessage;
    }

    /**
     * Sets result message.
     *
     * @param resultMessage the result message
     */
    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    /**
     * Gets status code.
     *
     * @return the status code
     */
    public HttpStatus getStatusCode() {
        return statusCode;
    }

    /**
     * Sets status code.
     *
     * @param statusCode the status code
     */
    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Gets return value.
     *
     * @return the return value
     */
    public T getReturnValue() {
        return returnValue;
    }

    /**
     * Sets return value.
     *
     * @param returnValue the return value
     */
    public void setReturnValue(T returnValue) {
        this.returnValue = returnValue;
    }

    /**
     * Instantiates a new Result response.
     *
     * @param status        the status
     * @param resultMessage the result message
     * @param statusCode    the status code
     * @param returnValue   the return value
     */
    public ResultResponse(char status, String resultMessage, HttpStatus statusCode, T returnValue) {
        this.status = status;
        this.resultMessage = resultMessage;
        this.statusCode = statusCode;
        this.returnValue = returnValue;
    }

    /**
     * Instantiates a new Result response.
     */
    public ResultResponse() {
    }
}
