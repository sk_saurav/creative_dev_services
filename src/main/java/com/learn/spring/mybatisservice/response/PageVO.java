/*
 * Copyright for PageVO.java by kumar since 8/16/20, 12:39 AM
 */

package com.learn.spring.mybatisservice.response;

/**
 * The type Page vo.
 *
 * @param <T> the type parameter
 */
public class PageVO<T> {
    private int pageCurr;
    private int pageSize;
    private int startPage;
    private int endpage;
    private long totalRows;
    private int totalPages;


    /**
     * Instantiates a new Page vo.
     */
    public PageVO() {
    }

    /**
     * Instantiates a new Page vo.
     *
     * @param pageCurr   the page curr
     * @param pageSize   the page size
     * @param totalRows  the total rows
     * @param totalPages the total pages
     * @param startPage  the start page
     * @param endpage    the endpage
     */
    public PageVO(int pageCurr, int pageSize, long totalRows, int totalPages, int startPage, int endpage) {
        this.pageCurr = pageCurr;
        this.pageSize = pageSize;
        this.totalRows = totalRows;
        this.totalPages = totalPages;
        this.startPage = startPage;
        this.endpage = endpage;
    }

    /**
     * Gets page curr.
     *
     * @return the page curr
     */
    public int getPageCurr() {
        return pageCurr;
    }

    /**
     * Sets page curr.
     *
     * @param pageCurr the page curr
     */
    public void setPageCurr(int pageCurr) {
        this.pageCurr = pageCurr;
    }

    /**
     * Gets page size.
     *
     * @return the page size
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Sets page size.
     *
     * @param pageSize the page size
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Gets total rows.
     *
     * @return the total rows
     */
    public long getTotalRows() {
        return totalRows;
    }

    /**
     * Sets total rows.
     *
     * @param totalRows the total rows
     */
    public void setTotalRows(long totalRows) {
        this.totalRows = totalRows;
    }

    /**
     * Gets total pages.
     *
     * @return the total pages
     */
    public int getTotalPages() {
        return totalPages;
    }

    /**
     * Sets total pages.
     *
     * @param totalPages the total pages
     */
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * Gets start page.
     *
     * @return the start page
     */
    public int getStartPage() {
        return startPage;
    }

    /**
     * Sets start page.
     *
     * @param startPage the start page
     */
    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    /**
     * Gets endpage.
     *
     * @return the endpage
     */
    public int getEndpage() {
        return endpage;
    }

    /**
     * Sets endpage.
     *
     * @param endpage the endpage
     */
    public void setEndpage(int endpage) {
        this.endpage = endpage;
    }

    @Override
    public String toString() {
        return "PageVO{" +
                "pageNum=" + pageCurr +
                ", pageSize=" + pageSize +
                ", startPage=" + startPage +
                ", endpage=" + endpage +
                ", totalRows=" + totalRows +
                ", totalPages=" + totalPages +
                '}';
    }
}
