/*
 * Copyright for PageResult.java by kumar since 8/16/20, 12:39 AM
 */

package com.learn.spring.mybatisservice.response;

import java.util.List;

/**
 * The type Page result.
 *
 * @param <T> the type parameter
 */
public class PageResult<T> {

    /**
     * The constant DEFAULT_OFFSET.
     */
    public static final long DEFAULT_OFFSET = 0;
    /**
     * The constant DEFAULT_MAX_NO_OF_ROWS.
     */
    public static final int DEFAULT_MAX_NO_OF_ROWS = 1000;
    private long totalElements;
    private PageVO pageVO;
    private final List<T> elements;
    private final Class <T> clazz;

    /**
     * Instantiates a new Page result.
     *
     * @param elements the elements
     * @param pageVO   the page vo
     * @param clazz    the clazz
     */
    public PageResult(List<T> elements, PageVO pageVO,Class clazz) {
        this.elements = elements;
        this.pageVO = pageVO;
        this.clazz = clazz;
    }

    /**
     * Instantiates a new Page result.
     *
     * @param totalElements the total elements
     * @param elements      the elements
     * @param pageVO        the page vo
     * @param clazz         the clazz
     */
    public PageResult(long totalElements, List<T> elements, PageVO pageVO, Class clazz) {
        this.totalElements = totalElements;
        this.elements = elements;
        this.pageVO = pageVO;
        this.clazz = clazz;
    }

    /**
     * Gets total elements.
     *
     * @return the total elements
     */
    public long getTotalElements() {
        return elements.size();
    }

    /**
     * Gets elements.
     *
     * @return the elements
     */
    public List<T> getElements() {
        return elements;
    }

    /**
     * Gets page vo.
     *
     * @return the page vo
     */
    public PageVO getPageVO() {
        return pageVO;
    }

    /**
     * Sets page vo.
     *
     * @param pageVO the page vo
     */
    public void setPageVO(PageVO pageVO) {
        this.pageVO = pageVO;
    }
}
