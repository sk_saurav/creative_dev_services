/*
 * Copyright for UserInfoDao.java by kumar since 9/6/20, 12:54 PM
 */

package com.learn.spring.mybatisservice.dao;

import com.learn.spring.mybatisservice.entity.ContentVO;
import com.learn.spring.mybatisservice.entity.IndusTypeInfo;
import com.learn.spring.mybatisservice.entity.UserInfoData;
import com.learn.spring.mybatisservice.entity.Users;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface User info dao.
 *
 * @author kumar
 * @project mybatis -service
 * @since 8 /2/2020
 */
@Repository
public interface UserInfoDao {
    /**
     * inserting to user_info_t
     *
     * @param userInfo the user info
     * @return int int
     */
    int insert(@Param("userInfo") UserInfoData userInfo);

    /**
     * returning all industry
     *
     * @return all industry
     */
    List<IndusTypeInfo> getAllIndustry();

    /**
     * getGeneratedKey
     *
     * @param users the users
     * @return generated key
     */
    int getGeneratedKey(Users users);

    /**
     * Gets all users.
     *
     * @return the all users
     */
    List<Users> getAllUsers();

    /**
     * Delete content by id int.
     *
     * @param contentID the content id
     * @return the int
     */
    int deleteContentById(String contentID);

    /**
     * Find valid content list.
     *
     * @param contentList the content list
     * @return the list
     */
    List<ContentVO> findValidContent(List<Integer> contentList);

    void updateContent(@Param("contentId") int contentId);
}
