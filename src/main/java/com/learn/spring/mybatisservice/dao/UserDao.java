/*
 * Copyright for UserDao.java by kumar since 26/09/20, 12:32 PM
 */

package com.learn.spring.mybatisservice.dao;

import com.learn.spring.mybatisservice.entity.UserInfo;
import com.learn.spring.mybatisservice.entity.UserMock;
import com.learn.spring.mybatisservice.entity.Users;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * The interface User dao.
 */
@Repository
public interface UserDao {

    /**
     * Save user info int.
     *
     * @param userInfo the user info
     * @return the int
     */
    int saveUserInfo(UserInfo userInfo);

    /**
     * Gets user info.
     *
     * @param user the user
     * @return the user info
     */
    UserInfo getUserInfo(UserInfo user);

    /**
     * Find all user list.
     *
     * @return the list
     */
    List<UserInfo> findAllUser();

    /**
     * Find user by id user info.
     *
     * @param userId the user id
     * @return the user info
     */
    UserInfo findUserById(@Param("id") Long userId);

    /**
     * Find user info list.
     *
     * @param userIds the user ids
     * @return the list
     */
    List<UserInfo> findUserInfo(@Param("ids") List<Long> userIds);

    /**
     * Gets user data.
     *
     * @param userId the user id
     * @return the user data
     */
    UserInfo getUserData(@Param("id") Long userId);

    /**
     * Gets image urls.
     *
     * @param imageIds the image ids
     * @return the image urls
     */
    List<String> getImageUrls(@Param("imageIds") List<String> imageIds);

    /**
     * Update db int.
     *
     * @param maps the maps
     * @return the int
     */
    int updateDB(@Param("maps") Map<Integer, List<String>> maps);

    /**
     * Find all user data list.
     *
     * @return the list
     */
    List<Users> findAllUserData();

    int deleteContent(@Param(value = "user") Users user);

    int deleteBatch(@Param(value = "users") List<Users> userList);

    List<UserMock> finUsers();

    void create(@Param(value = "users") List<UserMock> users);

    List<UserMock> finUsersByIds(@Param("iDs") List<String> iDs);

    int deleteMultiRecords(@Param("ids") List<UserMock> ids);
}
