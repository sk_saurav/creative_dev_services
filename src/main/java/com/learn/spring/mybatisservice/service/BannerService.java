/*
 * Copyright for BannerService.java by kumar since 8/16/20, 12:39 AM
 */

package com.learn.spring.mybatisservice.service;

import com.learn.spring.mybatisservice.entity.BannerXmlVO;
import com.learn.spring.mybatisservice.response.ResultResponse;
import java.io.FileNotFoundException;
import javax.xml.bind.JAXBException;

/**
 * Banner business logics
 *
 * @project mybatis -service
 * @uthor kumar
 * @since 8 /1/2020
 */
public interface BannerService {
    /**
     * Generate banner result response.
     *
     * @param banner the banner
     * @return the result response
     * @throws JAXBException         the jaxb exception
     * @throws FileNotFoundException the file not found exception
     */
    ResultResponse<BannerXmlVO> generateBanner(BannerXmlVO banner) throws JAXBException, FileNotFoundException;
}
