/*
 * Copyright for UserAddServiceImpl.java by kumar since 26/09/20, 5:51 PM
 */

package com.learn.spring.mybatisservice.service.impl;

import com.learn.spring.mybatisservice.dao.UserInfoDao;
import com.learn.spring.mybatisservice.entity.ContentVO;
import com.learn.spring.mybatisservice.entity.IndusTypeInfo;
import com.learn.spring.mybatisservice.entity.UserInfoData;
import com.learn.spring.mybatisservice.entity.Users;
import com.learn.spring.mybatisservice.response.ResultResponse;
import com.learn.spring.mybatisservice.service.UserAddService;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.WebApplicationContext;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

/**
 * The type User add service.
 *
 * @author kumar
 * @project mybatis -service
 * @since 8 /1/2020
 */
@Service
public class UserAddServiceImpl implements UserAddService {

    /**
     * The constant USER_CONTACT.
     */
    public static final String USER_CONTACT = "7870459937";
    /**
     * logger added as a service logger
     */
    private static final Logger logger = LoggerFactory.getLogger(UserAddServiceImpl.class);
    @Autowired
    private WebApplicationContext applicationContext;

    /**
     * DAO for inserting records
     *
     * @insert
     */
    @Autowired
    private UserInfoDao userInfoDao;
    /**
     * Gets common properties.
     *
     * @return the common properties
     */

    /**
     * add user info
     *
     * @param usersVo
     * @return
     */
    @Override
    public ResultResponse<Users> addUsers(List<Users> usersVo) throws Exception {
        logger.info(
                "Entering into UserAddServiceImpl:: addUsers with userVo {}", usersVo);
        if (CollectionUtils.isEmpty(usersVo)) {
            throw new Exception("body can not be empty..");
        }
        List<Future<Boolean>> futureList = getFuturesImage(usersVo);
        boolean result = false;
        for (Future futureResult : futureList) {
            result = (Boolean) futureResult.get();
            if (!result) {
                return new ResultResponse<>('F', "failed", HttpStatus.CREATED, null);
            }
        }

        return new ResultResponse<>('S', "success", HttpStatus.CREATED, null);
    }

    @NotNull
    private List<Future<Boolean>> getFuturesImage(List<Users> usersVo) {
        ExecutorService executorService = Executors.newFixedThreadPool(usersVo.size());
        UserInfoData infoData = null;
        UserServiceProcessor serviceProcessor = null;
        List<Future<Boolean>> futureList = new ArrayList<>();
        Future<Boolean> future = null;
        for (Users user : usersVo) {
            MessageFormat mf =
                    new MessageFormat(
                            "getUserContact: {0}, getUserEmail: {1}, getUserID: {2}, getUserID: {3}");
            System.out.println(
                    mf.format(
                            new Object[]{
                                    user.getUserContact(),
                                    user.getUserEmail(),
                                    user.getUserName(),
                                    user.getUserID()
                            }));
            logger.info(
                    "message formatter: >> ",
                    mf.format(
                            new Object[]{user.getUserContact(), user.getUserEmail(),
                                    user.getUserName(),
                                    user.getUserID()
                            }));
            infoData =
                    new UserInfoData(
                            user.getUserID(),
                            user.getUserName(),
                            user.getUserEmail(),
                            user.getUserContact());
            serviceProcessor =
                    (UserServiceProcessor)
                            applicationContext.getBean("userServiceProcessor", infoData);
            future = executorService.submit(serviceProcessor);
            futureList.add(future);
        }
        executorService.shutdown();
        return futureList;
    }

    /**
     * {@utility method}
     *
     * @return
     */
    @Override
    public Map<Integer, IndusTypeInfo> getAllIndustry() {
        List<IndusTypeInfo> indusTypeInfos = null;
        Map<Integer, IndusTypeInfo> indusTypeInfoMap = null;
        try {
            indusTypeInfos = userInfoDao.getAllIndustry();
            if (!CollectionUtils.isEmpty(indusTypeInfos)) {
                indusTypeInfoMap =
                        indusTypeInfos.stream()
                                .collect(
                                        toMap(
                                                IndusTypeInfo::getIndusTypeId,
                                                indusTypeInfo -> indusTypeInfo));
                /*industryInfo -> industryInfo,
                (oldvalue, newValue) -> oldvalue, ConcurrentHashMap::new));*/
            }
            if (!CollectionUtils.isEmpty(indusTypeInfoMap)) {
                return indusTypeInfoMap;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    /**
     * {@utility methods}
     *
     * @param users user details
     * @return user id
     */
    @Override
    public int getGeneratedKey(Users users) {
        users.setUserContact(USER_CONTACT);
        try {
            userInfoDao.getGeneratedKey(users);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users.getID();
    }

    /*@Override
    public ResponseEntity<Map<String, String>> getCommonProperties() {
        Map<String, String> configuration = new HashMap<>();
        configuration.put("nsp.app.id", commonProperties.getProperty("nsp.app.id"));
        configuration.put("obs.app.id", commonProperties.getProperty("obs.app.id"));
        configuration.put("spring.application.name", commonProperties.getProperty("spring.application.name"));
        return new ResponseEntity<>(configuration, HttpStatus.OK);
    }*/

    /**
     * Gets env.
     *
     * @param env the env
     * @return the env
     */
   /* @Override
    public String getEnv(String env) {
        return commonProperties.getProperty(env);
    }*/

    /**
     * Content delete response entity.
     *
     * @param contentID the content id
     * @return the response entity
     */
    @Override
    public ResponseEntity<String> contentDelete(String contentID) {
        if (ObjectUtils.isEmpty(contentID)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        int counter = userInfoDao.deleteContentById(contentID);
        if (counter == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    /**
     * Library content response entity.
     *
     * @param contentIDList the content id list
     * @return the response entity
     */
    @Override
    public ResponseEntity<ResultResponse<List<ContentVO>>> libraryContent(List<String> contentIDList) {
        List<Integer> contentList = contentIDList.stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        List<ContentVO> validContent = userInfoDao.findValidContent(contentList);
        validContent.forEach(content -> userInfoDao.updateContent(content.getContentID()));
        ResultResponse<List<ContentVO>> resultResponse = new ResultResponse<>('S', "success", HttpStatus.OK, validContent);
        return new ResponseEntity<>(resultResponse, HttpStatus.OK);
    }
}
