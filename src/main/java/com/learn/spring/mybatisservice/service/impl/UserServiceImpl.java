/*
 * Copyright for UserServiceImpl.java by kumar since 26/09/20, 12:32 PM
 */

package com.learn.spring.mybatisservice.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.learn.spring.mybatisservice.dao.UserDao;
import com.learn.spring.mybatisservice.entity.*;
import com.learn.spring.mybatisservice.response.PageResult;
import com.learn.spring.mybatisservice.response.PageVO;
import com.learn.spring.mybatisservice.response.ResultResponse;
import com.learn.spring.mybatisservice.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The type User service.
 */
@Service
public class UserServiceImpl implements UserService {
    /**
     * The constant API_EXECUTION_TIME.
     */
    public static final String API_EXECUTION_TIME = "API Execution Time = {}ms";
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private static final String RET_SUCC_MSG = "Success";
    private static final String RET_FAILURE_MSG = "fail";
    private static final String RET_SUCC_CODE = "200";
    private static final String RET_FAILURE_CODE = "99";
    private static final String RET_EXCEPTION_MESSAGE = "SQL fail to insert";
    private static final String RET_EXCEPTION_CODE = "SQL TRACE 1099";
    private final RestTemplate restTemplate;
    private final UserDao userDao;

    /**
     * Instantiates a new User service.
     *
     * @param restTemplate the rest template
     * @param userDao      the user dao
     */
    @Autowired
    public UserServiceImpl(RestTemplateBuilder restTemplate, UserDao userDao) {
        this.restTemplate = restTemplate.build();
        this.userDao = userDao;
    }

    @Override
    public Map<String, String> saveUserInfo(UserInfo userInfo) throws Exception {
        Map<String, String> ret = new HashMap<>();
        int counter;
        try {
            counter = userDao.saveUserInfo(userInfo);
            if (counter > 0) {
                ret.put("message", RET_SUCC_MSG);
                ret.put("status", RET_SUCC_CODE);
            } else {
                ret.put("message", RET_FAILURE_MSG);
                ret.put("status", RET_FAILURE_CODE);
            }
        } catch (Exception e) {
            ret.put("message", RET_EXCEPTION_MESSAGE);
            ret.put("status", RET_EXCEPTION_CODE);
        }

        return ret;
    }

    @Override
    public UserInfo getUserInfo(UserInfo user) {
        return userDao.getUserInfo(user);
    }

    /**
     * author saurav
     * description : to returns all user
     * date 21/06/2020
     *
     * @param pageSize page size
     * @param pageNum  page number
     * @return all users
     */
    @Override
    public PageResult<UserInfo> findAllUser(int pageSize, int pageNum) {
        Page page = PageHelper.startPage(pageNum, pageSize);
        List<UserInfo> userInfos = new ArrayList<>();
        try {
            userInfos = userDao.findAllUser();
        } catch (Exception e) {
            logger.info("Find User queries failed");
        }
        PageVO pageVO = getPageVO(pageSize, pageNum, page);
        PageResult<UserInfo> pageResult = new PageResult<>(userInfos, pageVO, UserInfo.class);
        return pageResult;
    }

    @NotNull
    private PageVO getPageVO(int pageSize, int pageNum, Page page) {
        PageVO pageVO = new PageVO();
        pageVO.setPageCurr(pageNum);
        pageVO.setPageSize(pageSize);
        pageVO.setStartPage(page.getStartRow() + 1);
        pageVO.setEndpage(page.getEndRow());
        pageVO.setTotalRows(page.getTotal());
        pageVO.setTotalPages(page.getPages());
        return pageVO;
    }

    /**
     * @param userId
     * @return userinfo
     * @author saurav
     */
    @Override
    public UserInfo findUserById(Long userId) {
        //return userDao.findUserById(userId);
        return userDao.getUserData(userId);
    }

    /**
     * @param userIds
     * @return userinfo
     * @author saurav
     */
    @Override
    public List<UserInfo> findUserInfo(List<Long> userIds) {
        return userDao.findUserInfo(userIds);
    }

    @Override
    @Async
    public CompletableFuture<User> findUserAsync(String user) throws InterruptedException {
        logger.info("Lookup " + user);
        String url = String.format("https://api.github.com/users/%s", user);
        User results = restTemplate.getForObject(url, User.class);
        Thread.sleep(1000L);
        return CompletableFuture.completedFuture(results);
    }

    @Override
    public List<String> findImageUrls(List<String> imageIds) {
        return userDao.getImageUrls(imageIds);
    }

    @Override
    public int updateDB(Map<Integer, List<String>> maps) {
        for (Map.Entry<Integer, List<String>> set : maps.entrySet()) {
            System.out.println(set.getKey() + " -> " + maps.values());
        }
        int data = 0;
        try {
            for (Map.Entry<Integer, List<String>> mapping : maps.entrySet())
                data = userDao.updateDB(maps);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * deleteContent Delete users
     *
     * @param userList the user list
     * @return
     */
    @Override
    public ResponseEntity<ResultResponse<String>> deleteContent(List<Users> userList) {
        Long startTime = System.currentTimeMillis();
        Long endTime = 0L;
        if (CollectionUtils.isEmpty(userList) && userList.size() == 0) {
            return new ResponseEntity<ResultResponse<String>>(new ResultResponse<String>('F', "Operation Failed", HttpStatus.BAD_REQUEST, null), HttpStatus.BAD_REQUEST);
        }
        /*int counter = userDao.deleteBatch(userList);
        if (counter == 0) {
            endTime = System.currentTimeMillis();
            logger.info("API Execution Time = " + (endTime - startTime) +"ms");
            return new ResponseEntity<ResultResponse<String>>(new ResultResponse<String>('S', "Operation Success", HttpStatus.NO_CONTENT, null ), HttpStatus.NO_CONTENT);
        }*/
        AtomicInteger updateCount = new AtomicInteger();
        try {
            userList.stream()
                    .forEach(user -> {
                        updateCount.set(userDao.deleteContent(user));
                    });
            if (updateCount.get() == 0) {
                endTime = System.currentTimeMillis();
                logger.info(API_EXECUTION_TIME, (endTime - startTime));
                return new ResponseEntity<ResultResponse<String>>(new ResultResponse<String>('S', "Operation Success", HttpStatus.NO_CONTENT, null), HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            endTime = System.currentTimeMillis();
            logger.info(API_EXECUTION_TIME, (endTime - startTime));
            return new ResponseEntity<ResultResponse<String>>(new ResultResponse<String>('F', "Operation Failed", HttpStatus.PRECONDITION_FAILED, e.getMessage()), HttpStatus.PRECONDITION_FAILED);
        }
        endTime = System.currentTimeMillis();
        logger.info(API_EXECUTION_TIME, (endTime - startTime));
        return new ResponseEntity<ResultResponse<String>>(new ResultResponse<String>('S', "Operation Success", HttpStatus.OK, null), HttpStatus.OK);
    }

    /**
     * Save user async completable future.
     *
     * @param file the file
     * @return the completable future
     */
    @Override
    @Async
    public CompletableFuture<List<UserMock>> saveUserAsync(MultipartFile file) throws IOException {
        List<UserMock> users = parseCSV(file);
        userDao.create(users);
        return CompletableFuture.completedFuture(users);
    }

    private List<UserMock> parseCSV(MultipartFile file) throws IOException {
        final List<UserMock> users = new ArrayList<>();
        try (final BufferedReader bf = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            String line;
            while ((line = bf.readLine()) != null) {
                String[] data = line.split(",");
                final UserMock userMock = new UserMock();
                /*userMock.setId(data[0]);
                userMock.setFirst_name(data[1]);
                userMock.setLast_name(data[2]);
                userMock.setEmail(data[3]);
                userMock.setGender(data[4]);
                userMock.setIp_address(data[5]);*/
                users.add(userMock);
            }
            logger.info("successfully crated users {}", users);
            return users;
        } catch (final IOException ex) {
            logger.error("Failed to parse CSV file {}", ex);
            throw new IOException("Failed to parse CSV file {}", ex);
        }
    }

    /**
     * Find all users completable future.
     *
     * @return the completable future
     */
    @Override
    @Async
    public CompletableFuture<List<UserMock>> findAllUsers() {
        List<UserMock> users = userDao.finUsers();
        return CompletableFuture.completedFuture(users);
    }

    /**
     * Delete users completable future.
     *
     * @param iDs the ds
     * @return the completable future
     */
    @Override
    @Async
    public CompletableFuture<ResponseEntity<ResultResponse<DeleteResponseVO>>> deleteUsers(List<String> iDs) {
        List<UserMock> mockUsers = userDao.finUsersByIds(iDs);
        return CompletableFuture.supplyAsync(() -> deleteRecords(mockUsers)).exceptionally(throwable -> handle(throwable));
    }

    private ResponseEntity<ResultResponse<DeleteResponseVO>> handle(Throwable throwable) {
        return ResponseEntity.badRequest().body(null);
    }

    private ResponseEntity<ResultResponse<DeleteResponseVO>> deleteRecords(List<UserMock> data) {
        ResultResponse resultResponse = new ResultResponse('S', "Success", HttpStatus.OK, null);
        DeleteResponseVO deleteResponseVO = new DeleteResponseVO();
        int deleteFlag = userDao.deleteMultiRecords(data);
        if (deleteFlag == data.size()) {
            deleteResponseVO.setCode("000");
            deleteResponseVO.setiD("000");
            deleteResponseVO.setDescription("seccess");
            resultResponse.setReturnValue(deleteResponseVO);
            return new ResponseEntity<>(resultResponse, HttpStatus.OK);
        } else {
            deleteResponseVO.setCode("999");
            deleteResponseVO.setiD("000");
            deleteResponseVO.setDescription("failed");
            resultResponse.setReturnValue(deleteResponseVO);
            return new ResponseEntity<>(resultResponse, HttpStatus.OK);
        }
    }
}
