/*
 * Copyright for OrderServiceImpl.java by kumar since 19/09/20, 8:48 PM
 */

package com.learn.spring.mybatisservice.service.impl;

import com.learn.spring.mybatisservice.entity.OrderHeadVO;
import com.learn.spring.mybatisservice.service.OrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author kumar
 * @project mybatis-service
 * @since 17-09-2020
 */
@Service
public class OrderServiceImpl implements OrderService {
    /**
     * Create order response entity.
     *
     * @param orderHeadVOList the order head vo list
     * @return the response entity
     */
    @Override
    public ResponseEntity<String> createOrder(List<OrderHeadVO> orderHeadVOList) {
        return ResponseEntity.ok("success");
    }
}
