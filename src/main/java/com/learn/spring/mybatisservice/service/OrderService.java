/*
 * Copyright for OrderService.java by kumar since 19/09/20, 8:48 PM
 */

package com.learn.spring.mybatisservice.service;

import com.learn.spring.mybatisservice.entity.OrderHeadVO;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * @author kumar
 * @project mybatis-service
 * @since 17-09-2020
 */
public interface OrderService {
    /**
     * Create order response entity.
     *
     * @param orderHeadVOList the order head vo list
     * @return the response entity
     */
    ResponseEntity<String> createOrder(List<OrderHeadVO> orderHeadVOList);
}
