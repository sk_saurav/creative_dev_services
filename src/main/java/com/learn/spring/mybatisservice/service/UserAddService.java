/*
 * Copyright for UserAddService.java by kumar since 26/09/20, 5:51 PM
 */

package com.learn.spring.mybatisservice.service;

import com.learn.spring.mybatisservice.entity.ContentVO;
import com.learn.spring.mybatisservice.entity.IndusTypeInfo;
import com.learn.spring.mybatisservice.entity.Users;
import com.learn.spring.mybatisservice.response.ResultResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

/**
 * user service to add user
 *
 * @author kumar
 * @project mybatis -service
 * @since 8 /1/2020
 */
public interface UserAddService {
    /**
     * addUsers to add user
     *
     * @param usersVo the users vo
     * @return result response
     * @throws Exception the exception
     */
    ResultResponse<Users> addUsers(List<Users> usersVo) throws Exception;

    /**
     * getAllIndustry
     *
     * @return all industry
     */
    Map<Integer, IndusTypeInfo> getAllIndustry();

    /**
     * getUserGenerated key
     *
     * @param users the users
     * @return generated key
     */
    int getGeneratedKey(Users users);

    /**
     * Gets common properties.
     *
     * @return the common properties
     */
    //ResponseEntity<Map<String, String>> getCommonProperties();

    /**
     * Gets env.
     *
     * @param env the env
     * @return the env
     */
    //String getEnv(String env);

    /**
     * Content delete response entity.
     *
     * @param contentID the content id
     * @return the response entity
     */
    ResponseEntity<String> contentDelete(String contentID);

    /**
     * Library content response entity.
     *
     * @param contentIDList the content id list
     * @return the response entity
     */
    ResponseEntity<ResultResponse<List<ContentVO>>> libraryContent(List<String> contentIDList);

}
