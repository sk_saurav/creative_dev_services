/*
 * Copyright for UserService.java by kumar since 26/09/20, 12:32 PM
 */

package com.learn.spring.mybatisservice.service;

import com.learn.spring.mybatisservice.entity.*;
import com.learn.spring.mybatisservice.response.PageResult;
import com.learn.spring.mybatisservice.response.ResultResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * The interface User service.
 */
public interface UserService {
    /**
     * Save user info map.
     *
     * @param userInfo the user info
     * @return the map
     * @throws Exception the exception
     */
    Map<String, String> saveUserInfo(UserInfo userInfo) throws Exception;

    /**
     * Gets user info.
     *
     * @param user the user
     * @return the user info
     */
    UserInfo getUserInfo(UserInfo user);

    /**
     * Find all user page result.
     *
     * @param pageSize the page size
     * @param pageNum  the page num
     * @return pageResult page result
     * @throws Exception the exception
     * @author saurav
     * @description : to returns all user
     * @date 21 /06/2020
     */
    PageResult<UserInfo> findAllUser(int pageSize, int pageNum)throws Exception;

    /**
     * Find user by id user info.
     *
     * @param userId the user id
     * @return userinfo user info
     * @author saurav
     */
    UserInfo findUserById(Long userId);

    /**
     * Find user info list.
     *
     * @param userIds the user ids
     * @return userinfo list
     * @author saurav
     */
    List<UserInfo> findUserInfo(List<Long> userIds);

    /**
     * Find user async completable future.
     *
     * @param user the user
     * @return the completable future
     * @throws InterruptedException the interrupted exception
     */
    CompletableFuture<User> findUserAsync(String user) throws InterruptedException;

    /**
     * Find image urls list.
     *
     * @param imageIds the image ids
     * @return the list
     */
    List<String> findImageUrls(List<String> imageIds);

    /**
     * Update db int.
     *
     * @param maps the maps
     * @return the int
     */
    int updateDB(Map<Integer, List<String>> maps);

    /**
     * Delete content response entity.
     *
     * @param userList the user list
     * @return the response entity
     */
    ResponseEntity<ResultResponse<String>> deleteContent(List<Users> userList);

    /**
     * Save user async completable future.
     *
     * @param file the file
     * @return the completable future
     */
    CompletableFuture<List<UserMock>> saveUserAsync(MultipartFile file) throws IOException;

    /**
     * Find all users completable future.
     *
     * @return the completable future
     */
    CompletableFuture<List<UserMock>> findAllUsers();

    /**
     * Delete users completable future.
     *
     * @param iDs the ds
     * @return the completable future
     */
    CompletableFuture<ResponseEntity<ResultResponse<DeleteResponseVO>>> deleteUsers(List<String> iDs);
}
